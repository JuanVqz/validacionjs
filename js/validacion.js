function validarForm() {
    
    var verificar = true;
    var expRegNombre = /^[a-zA-ZÑñÁáÉéÍíÓóÚúÜü\s]+$/;
    var expRegEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

    var formulario = document.getElementById("contacto-frm");
    var nombre = document.getElementById("nombre");
    var edad = document.getElementById("edad");
    var email = document.getElementById("email");
    var masculino = document.getElementById("M");
    var femenino = document.getElementById("F");
    var asunto = document.getElementById("asunto");
    var comentarios = document.getElementById("comentarios");
    
    if (!nombre.value){
        console.log("El campo nombre es requerido");
        nombre.focus();
        verificar = false;
    } else if (!expRegNombre.exec(nombre.value)){
        console.log("El campo nombre solo acepta letras y espacios");
        nombre.focus();
        verificar = false;
    } else if (!edad.value){
        console.log("El campo edad es requerido");
        edad.focus();
        verificar = false;
    } else if (isNaN(edad.value)){//isNotaNumber
        console.log("El campo edad solo acepta numeros");
        email.focus();
        verificar = false;
    } else if (edad.value < 18 || edad.value > 60){
        console.log("Debes estar en el rango de edad");
        edad.focus();
        verificar = false;
    } else if (!email.value){
        console.log("El campo email es requerido");
        email.focus();
        verificar = false;
    } else if (!expRegEmail.exec(email.value)){
        console.log("El campo email no es valido");
        email.focus();
        verificar = false;
    } else if (!masculino.checked && !femenino.checked){
        console.log("El campo sexo es requerido");
        femenino.focus();
        verificar = false;
    }else if (!asunto.value){
        console.log("El campo asunto es requerido");
        asunto.focus();
        verificar = false;
    } else if(comentarios.value.lenght > 255){
        console.log("El campo comentario es muy largo");
        comentarios.focus();
        verificar = false;
    }

    if(verificar){
        console.log("se envio el form");
        //document.contacto_frm.submit();
    }
}

function limpiarForm(){
    alert("limpando");
    document.getElementById("contacto-frm").reset();
}

window.onload = function() {

    var botonEnviar, botonLimpiar;

    botonLimpiar = document.getElementById("limpiar");
    botonLimpiar.onclick = limpiarForm;

    botonEnviar = document.contacto_frm.enviar_btn;
    botonEnviar.onclick = validarForm;
}
